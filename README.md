Let's Crash Cameleon!
======================================

Set up the package
------------------

First we need to install a few things

```
mkdir neuralNetJSS neuralNetJSS/build neuralNetJSS/src
cd neuralNetJSS/src

# Clone some repositories
git clone https://github.com/UCATLAS/xAODAnaHelpers.git
git clone https://gitlab.cern.ch/atlas-physics/sm/photons-jets/multijetsamples.git

# Of course, this package as well
git clone https://gitlab.com/csauer/crachCameleon

```

Now we set up the ATLAS software environment and compile

```
# If you're outside of LXPlus
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# Set up the ATLAS software environment
setupATLAS

# set up ATLAS Analysis Release with a recent version of cmake
asetup AnalysisBase 21.2.189,here --cmakeversion=3.20.2

# Start compiling
cd ../build
cmake ../src
make

# Don't forget to set up all path variables
source x86_64-*/setup.sh
```

Run a quick test
----------------

I've converted and EFN as well as an PFN (from https://energyflow.network/) to the ONNX format and added those to this repository for testing purposes. The models are untrained; hence, dont expect anything meaningful :).

```
# Test EFN
./x86_64-centos7-gcc8-opt/bin/testONNX_EFN ../src/deepSetsJSS/share/models/efn.onnx

#============================================
# [INFO] Star test of ONNXModel (EFN) class
# [INFO] This model has the following inputs:
#        |- zs_input
#        |- phats_input
# [INFO] This model has the following outputs:
#        |- activation_6
# [INFO] Running inference ...
# [INFO] PFN Model prediction(s):
#        (0) [0.512946, 0.487054] 
#============================================

# Test PFN
./x86_64-centos7-gcc8-opt/bin/testONNX_PFN ../src/deepSetsJSS/share/models/pfn.onnx

# ============================================
# [INFO] Star test of ONNXModel (PFN) class
# [INFO] This model has the following inputs:
#        |- input
# [INFO] This model has the following outputs:
#        |- activation_6
# [INFO] Running inference ...
# [INFO] PFN Model prediction(s):
#        (0) [0.0192249, 0.980775] 
# ============================================
```

Start actual stress-test
------------------------

```
# Don't forget to set up all path variables and make binaries available
source build/x86_64-*/setup.sh

PATH2DATA=/user/csauer/dijetXSMsmt/ana/dijetXSMeas/samples/samplelists/Pythia8EvtGen/MC16a

# Files to process (many of them!)
FILE_LISTS=$(find $PATH2DATA -type f -name "*ANALYSIS*" ! -name "*JZ0_*" ! -name "*JZ1_*" -iname "*.list" -exec cat {} \;)

# Start!
trainTree -o output.root $FILE_LISTS
```
