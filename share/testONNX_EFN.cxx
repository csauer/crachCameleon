/*
  Christof Sauer <csauer@cern.ch>

  Run a quick test for an EFN
*/

// System include(s):
#include <iostream>
#include <string>
#include <assert.h>

// ONNX-related includes
#include <deepSetsJSS/ONNXModel.h>


int main(int argc, char** argv)
{

  //==============================================
  //
  // Just some checks; nothing interesting here
  //
  //==============================================

  // Check inputs
  assert(argc==2);

   // Get path to model
   std::string path2model(argv[1]);
  // Give some feedback
  std::cout << "[INFO] Star test of ONNXModel (PFN) class" << std::endl;

  //==============================================
  //
  // Load the ONNX model and get a prediction
  //
  //==============================================

  // Initialize ONNX model
  ONNXModel model(path2model);

  // Let's get a prediction
  int N = 1;

  // First input to the model
  int input_feature_size_zs = 1;
  std::vector<int64_t> input_node_dims_zs = {N, input_feature_size_zs};
  std::vector<float> input_tensor_values_zs = {0.8394};

  // Second input to the model
  int input_feature_size_phats = 2;
  std::vector<int64_t> input_node_dims_phats = {1, N, input_feature_size_phats};
  std::vector<float> input_tensor_values_phats = {0.4394, 1.2045};

  // Output of the model
  int output_feature_size = 2;
  std::vector<int64_t> output_node_dims = {N, output_feature_size};

  // Start predicting
  std::cout << "[INFO] Running inference ..." << std::endl;
  std::vector<float> output = model.RunInference
  (
    input_tensor_values_zs, input_node_dims_zs,
    input_tensor_values_phats, input_node_dims_phats,
    output_node_dims
  );

  //==============================================
  //
  // print the result(s)
  //
  //==============================================

  // Print the output
  std::cout << "[INFO] EFN Model prediction(s):" << std::endl;
  int64_t count = 0;
  for (int64_t i=0; i<output_node_dims[0]; ++i)
  {
    std::cout << "       (" << i << ") [";
    for (int64_t i=0; i<output_node_dims[1]; ++i)
    {
      std::cout << output[count];
      if (i==(output_node_dims[1]-1))
      {
        std::cout << "] " << std::endl;
      }
      else
      {
        std::cout << ", ";
      }
      count++;
    }
    count += output_node_dims[1];
  }

  return 0;
}
















