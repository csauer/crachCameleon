/*
  Christof Sauer <csauer@cern.ch>

  Run a quick test for an EFN
*/

// System include(s):
#include <iostream>
#include <string>
#include <assert.h>

// ONNX-related includes
#include <deepSetsJSS/ONNXModel.h>


int main(int argc, char** argv)
{

  //==============================================
  //
  // Just some checks; nothing interesting here
  //
  //==============================================

  // Check inputs
  assert(argc==2);

  // Get path to model
  std::string path2model(argv[1]);
  // Give some feedback
  std::cout << "[INFO] Star test of ONNXModel (PFN) class" << std::endl;

  //==============================================
  //
  // Load the ONNX model and get a prediction
  //
  //==============================================

  // Initialize ONNX model
  ONNXModel model(path2model);

  // Let's get a prediction
  int N = 2;

  // Input data for the PFN
  int input_feature_size = 4;
  std::vector<int64_t> input_node_dims = {1, N, input_feature_size};
  // This vector holds the actual input
  std::vector<float> input_tensor_values = {
     -0.8394,  0.1673,  2.4556, -2.1247, +0.8394,  -0.1673,  -2.4556, +2.1247};

  int output_feature_size = 2;
  std::vector<int64_t> output_node_dims = {1, output_feature_size};

  // Start predicting
  std::cout << "[INFO] Running inference ..." << std::endl;

  std::vector<float> output = model.RunInference
  (
    input_tensor_values, input_node_dims,
    output_node_dims
  );

  //==============================================
  //
  // print the result(s)
  //
  //==============================================

  // Print the output
  std::cout << "[INFO] PFN Model prediction(s):" << std::endl;
  int64_t count = 0;
  for (int64_t i=0; i<output_node_dims[0]; ++i)
  {
    std::cout << "       (" << i << ") [";
    for (int64_t i=0; i<output_node_dims[1]; ++i)
    {
      std::cout << output[count];
      if (i==(output_node_dims[1]-1))
      {
        std::cout << "] " << std::endl;
      }
      else
      {
        std::cout << ", ";
      }
      count++;
    }
    count += output_node_dims[1];
  }

  return 0;
}
















