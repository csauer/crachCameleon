// System include(s):
#include <iostream>
#include <string>
#include <vector>
#include <assert.h>

// Argument parser
#include <externals/argparse.h>

// Import for this class
#include "deepSetsJSS/TrainTree.h"


int main(int argc, char** argv)
{

  //==================================
  //
  // Commandline parsing
  //
  //==================================

  argparse::ArgumentParser args("TrainTreeMaker");

  args.add_argument("fin")
    .default_value<std::vector<std::string>>({})
    .help("Files to read.")
    .remaining();

  args.add_argument("-n", "--nevents")
    .default_value(0)
    .required()
    .help("Numbe rof events to read from this file.")
    .scan<'i', int>();

  args.add_argument("-o", "--fout")
    .default_value(std::string("output.root"))
    .required()
    .help("Name of the output file.");

  args.add_argument("-t", "--treename")
    .default_value(std::string("TrackJSSTrees_JSS"))
    .required()
    .help("Name of the Tree to read from file(s).");

  // Some quick checks
  try {
    args.parse_args(argc, argv);
  }
  catch (const std::runtime_error& err) {
    std::cerr << err.what() << std::endl;
    std::cerr << args;
    std::exit(1);
  }

  //==================================
  //
  // Start making the tree
  //
  //==================================

  // Initialize train tree
  TrainTree trainTree;

  // Add files to read
  trainTree.AddFile( args.get<std::vector<std::string>>("fin") );

  // Name of the trees to read from file(s)
  trainTree.SetTreeName( args.get<std::string>("--treename") );

  // Set the number of events to read from this file
  trainTree.SetNEvents(args.get<int>("--nevents"));

  // Set name of the output file
  trainTree.SetOutFileName( args.get<std::string>("--fout") );

  // Start!
  trainTree.Start();

//  // Some commandline arguments
//  std::string fout;
//  std::vector<std::string> fin;
//
//  //==================================
//  //
//  // Commandline parsing
//  //
//  //==================================
//
//  assert(argc==3);
//  fout  = std::string(argv[1]);
//  for (int i=2; i<argc; ++i)
//  {
//    fin.push_back(std::string(argv[i]));
//  
//
//  //==================================
//  //
//  // Do shuffeling and re-weighting
//  //
//  //==================================
//
//  Afterburner afterburner("tree", fin, fout);
//
//  // Run the afterburner
////  afterburner.Split(5);
//  afterburner.Start();

  return 0;
}


