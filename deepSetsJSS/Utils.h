#ifndef UTILS_H
#define UTILS_H

// Standard imports
#include <iostream>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <time.h>


namespace utils
{

  // Creates a directory for saving the plots
  inline void makedir (std::string outDir)
  {
    if(!opendir(outDir.c_str()))
    {
      std::cout << "Creating directory " << outDir.c_str() << std::endl;
      const int dir_err = mkdir(outDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      if (-1 == dir_err){
        std::cout << "Error creating directory " << outDir.c_str() << std::endl;
        exit(1);
      }
    }
  }


  inline std::string hex_string(int length)
  {
    // Output string
    std::string out_str;

    // Set seed
    srand(time(0));
    //hexadecimal characters
    char hex_characters[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

    for(int i=0; i<length; i++)
    {
      out_str.append(std::string(1, hex_characters[rand() % 16]));
    }
    return out_str;
  }


  inline std::vector<std::string> drop_cols (std::vector<std::string> cols)
  {
    std::vector<std::string> cols2keep(cols);
    const std::vector<std::string> cols2drop = {"__"};
    cols2keep.erase(std::remove_if(
      cols2keep.begin(), cols2keep.end(),
      [&cols2drop](const std::string & x) {
        return std::any_of(cols2drop.begin(), cols2drop.end(), [&x](std::string drop){ return x.find(drop) != std::string::npos;} );
      }), cols2keep.end());
    return cols2keep;
  }

  template<typename T>
  inline std::vector<std::vector<T>> chuck_vector (const std::vector<T>& vec, size_t n)
  {
    std::vector<std::vector<T>> outVec;
    size_t length = vec.size() / n;
    size_t remain = vec.size() % n;
    size_t begin = 0;
    size_t end = 0;
    for (size_t i = 0; i < std::min(n, vec.size()); ++i)
    {
        end += (remain > 0) ? (length + !!(remain--)) : length;
        outVec.push_back(std::vector<T>(vec.begin() + begin, vec.begin() + end));
        begin = end;
    }
    return outVec;
  }

  template<typename T>
  inline std::vector<std::vector<T>> split_train_test_val (const std::vector<T>& order, const float fTrain, const float fTest)
  {
    // Return vector
    std::vector<std::vector<T>> rvector;

    // Get position vectors
    auto first = order.begin();
    auto last = order.begin();

    // Range parameters
    int start = 0, stop = static_cast<int>(fTrain * order.size());

    // Get new vectors
    // - Training
    rvector.push_back(std::vector<T>(first + start, first + stop));
    // - Test
    start += stop + 1;
    stop  += static_cast<int>((fTest) * order.size());
    rvector.push_back(std::vector<T>(first + start, first + stop));
    // - Validation
    start  += static_cast<int>((1 - fTrain - fTest) * order.size());
    if (start >= order.size())
    {
      rvector.push_back(std::vector<T>());
      return rvector;
    }
    rvector.push_back(std::vector<T>(first + start, order.end()));

    return rvector;
  }

}

#endif // UTILS_H
