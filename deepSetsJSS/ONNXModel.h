/*
  Christof Sauer <csauer@cern.ch>, January, 25, 2022

  Basic model class to load model trained with keras/tensorflow
*/


#ifndef ONNXModel_H
#define ONNXModel_H

// AnaAlgorithm includes
#include <AsgTools/MessageCheck.h>
#include <AnaAlgorithm/AnaAlgorithm.h>

// System include(s).
#include <memory>
#include <string>
#include <iostream>

// ONNX Runtime include(s).
#include <core/session/onnxruntime_cxx_api.h>


class ONNXModel
{

  public:

    // This is a standard algorithm constructor
    ONNXModel (const std::string & path2model);

    // This function makes a prediction :)
    std::vector<float> RunInference
    (
      // Inputs
      std::vector<float> input_tensor_values,
      std::vector<int64_t> input_node_dims,
      // Output
      std::vector<int64_t> output_node_dims
    );

    std::vector<float> RunInference
    (
      // zs inputs
      std::vector<float> input_tensor_values_zs,
      std::vector<int64_t> input_node_dims_zs,
      // phats inputs
      std::vector<float> input_tensor_values_phats,
      std::vector<int64_t> input_node_dims_phats,
      // Output
      std::vector<int64_t> output_node_dims
    );

  private:

    // Those are the the names of the input and output nodes
    // of the trained model
    std::vector<const char*> input_node_names;
    std::vector<const char*> output_node_names;

    // This will be our session that remains active
    // the entire time
    std::unique_ptr< Ort::Session > m_session;
    std::unique_ptr< Ort::Env > m_env;

};


#endif // ONNXModel_H

