#ifndef PREPROCESSING_H
#define PREPROCESSING_H

// Standard imports
#include <fstream>
#include <numeric>
#include <vector>
#include <cmath>
#include <iterator>
#include <algorithm>

// ROOT-related includes
#include "TMath.h"


namespace preprocessing
{

  template<typename T>
  std::vector<int> sort_index (const std::vector<T> & values)
  {
    std::vector<int> index(values.size());
    std::iota(index.begin(), index.end(), 0);
    std::sort(index.begin(), index.end(), [&values](uint8_t a, uint8_t b) { return values[a] > values[b]; } );
    return index;
  }


  template<typename T>
  std::vector<T> sort_wrt (const std::vector<T> & x, const std::vector<T> & ref)
  {
    // Get sorting index
    std::vector<int> sort_idx = sort_index(ref);

    // Sorted vector
    std::vector<T> x_sorted;

    // Now sort
    for(auto i : sort_idx)
    {
      x_sorted.push_back(x[i]);
    }

    return x_sorted;
  }


  template<typename T>
  std::vector<T> shift_phi(const std::vector<T> & phi, const std::vector<T> & w)
  {
    // Make a copy of the input container
    std::vector<T> phi_shift(phi);

    // Compute total weights
    float wTot = std::accumulate(w.begin(), w.end(), 0);

    // Get mean
    float x = 0;
    float y = 0;
    for (unsigned int i=0; i<phi.size(); i++)
    {
      x += (w[i]/wTot) * TMath::Cos(phi[i]);
      y += (w[i]/wTot) * TMath::Sin(phi[i]);
    }
    // Quick check
    if ((x==0) && (y==0))
    {
      for (unsigned int i=0; i<phi.size(); i++)
      {
        phi_shift[i] = 0;
      }
      return phi_shift;
    }
    // Compute the mean
    float phi_mean = TMath::ATan2(y, x);
    // Now shift
    for (unsigned int i=0; i<phi.size(); i++)
    {
      phi_shift[i] = static_cast<T>(std::fmod((phi[i] - phi_mean) + TMath::Pi()  * 3, 2 * TMath::Pi() ) - TMath::Pi() );
    }
    return phi_shift;
  }


  template<typename T>
  std::vector<T> shift_eta(const std::vector<T> & eta, const std::vector<T> & w)
  {
    // Compute total sum of weights and a scalar product
    float wTot = std::accumulate(w.begin(), w.end(), 0);
    float dotp = std::inner_product(std::begin(eta), std::end(eta), std::begin(w), 0.0);

    // Compute mean value
    float mu = dotp / wTot;

    // Output containers
    std::vector<T> eta_shift(eta);

    // Do the subtraction
    for(auto & el : eta_shift)
    {
      el -= mu;
    }

    return eta_shift;
  }


  template<typename T>
  std::vector<T> norm(const std::vector<T> & x, const std::vector<T> & w)
  {
    // Compute total weights
    float wTot = std::accumulate(w.begin(), w.end(), 0);

    // Normalize vector
    std::vector<T> x_norm(x);

    for(unsigned int i=0; i<x_norm.size(); ++i)
    {
      x_norm[i] /= wTot;
    }

    return x_norm;
  }


  template<typename T>
  float pca_angle (const std::vector<T> & eta, const std::vector<T> & phi, const std::vector<T> & e)
  {
    // Shift eta and phi of the constituents accordingly
    std::vector<T> deta(shift_eta(eta, e));
    std::vector<T> dphi(shift_phi(phi, e));

    // Compute total weights
    float e_tot = std::accumulate(e.begin(), e.end(), 0);

    // Now, all constituents are centered around (0,0) within a range of [-R,R]x[-R,R]
    // 1. Get the means of eta and phi in tansformed system
    float mu_eta = std::inner_product(std::begin(deta), std::end(deta), std::begin(e), 0.0) / e_tot;
    float mu_phi = std::inner_product(std::begin(dphi), std::end(dphi), std::begin(e), 0.0) / e_tot;

    // 2. Get the second moment of eta and phi needed for variance
    std::vector<T> eta2(deta);
    for (auto & x : eta2) x*=x;
    std::vector<T> phi2(dphi);
    for (auto & x : phi2) x*=x;

    // Mean
    float mu_eta2 = std::inner_product(std::begin(eta2), std::end(eta2), std::begin(e), 0.0) / e_tot;
    float mu_phi2 = std::inner_product(std::begin(phi2), std::end(phi2), std::begin(e), 0.0) / e_tot;

    // 3. For correlation matrix
    std::vector<T> detaXdphi(deta);
    for (unsigned int i=0; i<detaXdphi.size(); ++i)
    {
      detaXdphi[i] *= dphi[i];
    }
    float mu_eta_phi = std::inner_product(std::begin(detaXdphi), std::end(detaXdphi), std::begin(e), 0.0) / e_tot;

    // Compute the standard deviations
    float sig_eta2 = mu_eta2 - mu_eta * mu_eta;
    float sig_phi2 = mu_phi2 - mu_phi * mu_phi;
    float sig_eta_phi = mu_eta_phi - mu_eta * mu_phi;

    // Solve the eigenvalue problem and compute the characteristic polynomial
    float lam_neg = 0.5 * (sig_eta2 + sig_phi2 - TMath::Sqrt((sig_eta2 - sig_phi2) * (sig_eta2 - sig_phi2) + 4 * sig_eta_phi * sig_eta_phi));

    // Get direction of first PCA
    float first_pca_eta = sig_eta2 + sig_eta_phi - lam_neg;
    float first_pca_phi = sig_phi2 + sig_eta_phi - lam_neg;


    // The sign ofthe first PCA is ambiguous; let it point in the direction of highest energy
    // Do the projction
    std::vector<T> proj(eta.size());
    for (unsigned int i=0; i<proj.size(); ++i)
    {
      proj[i] = first_pca_eta * deta[i] + first_pca_phi * dphi[i];
    }

    // Checjk which side has more energy
    float energy_up = 0;
    float energy_dn = 0;

    for (unsigned int i=0; i<proj.size(); ++i)
    {
      if (proj[i] >  0) energy_up += e[i];
      if (proj[i] <= 0) energy_dn += e[i];
    }

    if (energy_dn < energy_up)
    {
      first_pca_eta *= -1;
      first_pca_phi *= -1;
    }

    // Compute the rotation angle by which to rotate the constituents in eta-phi space
    float alpha = TMath::Pi() / 2. + atan(first_pca_phi / first_pca_eta);

    // Take care of discontinuity
    if (TMath::Cos(alpha) * first_pca_phi > TMath::Sin(alpha) * first_pca_eta)
      alpha -= TMath::Pi();

    return (-1)*alpha;
  }


  template<typename T>
  int parity (const std::vector<T> & eta, const std::vector<T> & e)
  {
    float energy_pos = 0;
    float energy_neg = 0;

    for (unsigned int i=0; i<eta.size(); ++i)
    {
      if (eta[i] <= 0) energy_neg += e[i];
      if (eta[i] >  0) energy_pos += e[i];
    }

    return (energy_pos < energy_neg) ? -1 : 1;
  }


  template<typename T>
  std::vector<T> flip(const std::vector<T> & x, const int & par)
  {
    std::vector<T> out(x);
    for (auto & el : out)
    {
      el *= par;
    }
    return out;
  }


  template<typename T>
  std::vector<T> rot_eta (const std::vector<T> & x, const std::vector<T> & y, const float & angle)
  {
    std::vector<T> x_rot(x);
    for (unsigned int ii = 0; ii < x.size(); ++ii)
      x_rot[ii] = x[ii]*TMath::Cos(angle) - y[ii]*TMath::Sin(angle);
    return x_rot;
  }


  template<typename T>
  std::vector<T> rot_phi (const std::vector<T> & x, const std::vector<T> & y, const float & angle)
  {
    std::vector<T> y_rot(y);
    for (unsigned int ii = 0; ii < y.size(); ++ii)
      y_rot[ii] = x[ii]*TMath::Sin(angle) + y[ii]*TMath::Cos(angle);
    return y_rot;
  }


  template<typename T>
  bool noNaNIsFinite (const std::vector<T> & x)
  {
    return !std::any_of(x.begin(), x.end(),  [](T item) { return (!TMath::Finite(item) || TMath::IsNaN(item)); });
  }
}


#endif // PREPROCESSING_H
