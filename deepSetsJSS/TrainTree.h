#ifndef TRAINTREE_H
#define TRAINTREE_H


// System include(s).
#include <iostream>
#include <string>
#include <memory>
#include <vector>

// ROOT libraries
#include "ROOT/RDataFrame.hxx"
#include "TRandom3.h"


class TrainTree
{

  // Make some handy abbreviations
  typedef ROOT::RDF::RNode RNode;
  typedef ROOT::RDF::TH1DModel TH1DModel;
  typedef ROOT::RDF::TH2DModel TH2DModel;
  typedef std::vector<float> v_f;
  typedef std::vector<std::vector<float> > v_v_f;
  typedef ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>> SnapRet_t;

  public:

    // This is a standard algorithm constructor
    TrainTree ();
    ~TrainTree ();

    // Add files to be read by RDF
    void AddFile (const std::string & fName);
    void AddFile (const std::vector<std::string> & fNames);

    // Set event selection
    void AddCut (const std::string & cut);
    void AddCut (const std::vector<std::string> & cuts);

    // Set the name of the tree to read from files
    void SetTreeName (const std::string & treeName);

    // Set the number of events to keep
    void SetNEvents (const int & nEvents);

    // Book some histograms
    void BookHisto (const TH1DModel & model, const std::string col, const std::string w);
    void BookHisto (const TH1DModel & model, const std::string col1, const std::string col2, const std::string w);

    // For the generated file
    void SetOutFileName (const std::string & fName);

    // Start building the trees
    void Start ();

  private:

    // Modifier functions for RDF
    auto BasicFilter (RNode node, TRandom3 & rand);
    auto ApplyDefine (RNode node, const int & jetIdx);
    auto MkSnapshots (RNode node1, RNode node2);
    // Read booked histograms
    void ReadHistos ();

  private:

    // THe number of events to keep
    int m_nEvents;

    // The list of files to read
    std::vector<std::string> m_fNames;

    // The list of string-based cuts
    std::vector<std::string> m_cuts;

    // List of columns in output file
    std::vector<std::string> m_cols, m_branches;

    // Name of the input tree and the output file
    std::string m_treeName, m_fOutName;

};


#endif // TRAINTREE_H

