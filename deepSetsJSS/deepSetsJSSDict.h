#ifndef NeuralNetJSS_LINKDEF_H
#define NeuralNetJSS_LINKDEF_H

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif

#include <deepSetsJSS/TrainTreeAlgo.h>
#include <deepSetsJSS/ONNXModel.h>
#include <deepSetsJSS/TrainTree.h>

// Some common definitions:
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// Declare the class(es) to generate dictionaries for:
#pragma link C++ class TrainTreeAlgo+;
//#pragma link C++ class Afterburner+;
//#pragma link C++ class ONNXModel+;

#endif // NeuralNetJSS_LINKDEF_H
