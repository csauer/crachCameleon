
// Standard includes
#include <vector>
#include <numeric> // Accumulate

// Local include(s).
#include "deepSetsJSS/ONNXModel.h"
#include "PathResolver/PathResolver.h"


template <typename T>
T vectorProduct(const std::vector<T>& v)
{
    return std::accumulate(v.begin(), v.end(), 1, std::multiplies<T>());
}


ONNXModel :: ONNXModel (const std::string & path2model)
{
  // This is the path (+fname) to the ONNX model file
  std::string fullPathToFile = PathResolverFindCalibFile(path2model.c_str());

  // Load the onnx model to memory using the path m_path_to_onnx
  m_env = std::make_unique< Ort::Env >(ORT_LOGGING_LEVEL_WARNING, "");

  // Initialize session options if needed
  Ort::SessionOptions session_options;
  session_options.SetIntraOpNumThreads(1);
  session_options.SetGraphOptimizationLevel(GraphOptimizationLevel::ORT_ENABLE_EXTENDED);

  // Initialize the session wit the requested model
  Ort::AllocatorWithDefaultOptions allocator;
  m_session = std::make_unique< Ort::Session >(*m_env, fullPathToFile.c_str(), session_options);

  // Retirive the input node(s)
  size_t num_input_nodes = m_session->GetInputCount();

  // Iterate over all input nodes
  std::cout << "[INFO] This model has the following inputs:" << std::endl;
  for (std::size_t i = 0; i<num_input_nodes; i++)
  {
    char* input_name = m_session->GetInputName(i, allocator);
    std::cout << "       |- " << input_name << std::endl;
    input_node_names.push_back(input_name);
  }

  // Now, get the output nodes
  size_t num_output_nodes = m_session->GetOutputCount();

  // Iterate over all output nodes
  std::cout << "[INFO] This model has the following outputs:" << std::endl;
  for(std::size_t i = 0; i < num_output_nodes; i++ )
  {
    char* output_name = m_session->GetOutputName(i, allocator);
    std::cout << "       |- " << output_name << std::endl;
    output_node_names.push_back(output_name);
  }

};


//=====================================================================================
//
// @ Matt
//
// TODO: We could easily just write one inference function for both, EFM and PFN, but
// in this case I decided just to do it separately
//
// ====================================================================================


// For PFN
std::vector<float> ONNXModel::RunInference
(
  // zs inputs
  std::vector<float> input_tensor_values,
  std::vector<int64_t> input_node_dims,
  // Output
  std::vector<int64_t> output_node_dims
){

  // Prepare container for input and output
  std::vector<Ort::Value> inputTensors, outputTensors;

  // Create input tensor object from data values
  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

  // Initialize the tensors
  // - The input(s)
  inputTensors.push_back(Ort::Value::CreateTensor<float>(
    memory_info,
    input_tensor_values.data(),
    input_tensor_values.size(),
    input_node_dims.data(),
    input_node_dims.size()
  ));

  size_t outputTensorSize = vectorProduct(output_node_dims);
  std::vector<float> outputTensorValues(outputTensorSize);
   // For the output
  outputTensors.push_back(Ort::Value::CreateTensor<float>(
    memory_info,
    outputTensorValues.data(),
    outputTensorSize,
    output_node_dims.data(),
    output_node_dims.size()
  ));

  // Run the session and het the prediction by the model
  m_session->Run
  (
    Ort::RunOptions{nullptr},
    input_node_names.data(),
    inputTensors.data(),
    input_node_names.size(),
    output_node_names.data(),
    outputTensors.data(),
    output_node_names.size()
  );

  return outputTensorValues;

}


// For EFN
std::vector<float> ONNXModel::RunInference
(
  // zs inputs
  std::vector<float> input_tensor_values_zs,
  std::vector<int64_t> input_node_dims_zs,
  // phats inputs
  std::vector<float> input_tensor_values_phats,
  std::vector<int64_t> input_node_dims_phats,
  // Output
  std::vector<int64_t> output_node_dims
){

  // Prepare container for input and output
  std::vector<Ort::Value> inputTensors, outputTensors;

  // Create input tensor object from data values
  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

  // Initialize the tensors
  // - The zs input(s)
  inputTensors.push_back(Ort::Value::CreateTensor<float>(
    memory_info,
    input_tensor_values_zs.data(),
    input_tensor_values_zs.size(),
    input_node_dims_zs.data(),
    input_node_dims_zs.size()
  ));

  // - The phats input(s)
  inputTensors.push_back(Ort::Value::CreateTensor<float>(
    memory_info,
    input_tensor_values_phats.data(),
    input_tensor_values_phats.size(),
    input_node_dims_phats.data(),
    input_node_dims_phats.size()
  ));

  size_t outputTensorSize = vectorProduct(output_node_dims);
  std::vector<float> outputTensorValues(outputTensorSize);
   // For the output
  outputTensors.push_back(Ort::Value::CreateTensor<float>(
    memory_info,
    outputTensorValues.data(),
    outputTensorSize,
    output_node_dims.data(),
    output_node_dims.size()
  ));

  // Run the session and het the prediction by the model
  m_session->Run
  (
    Ort::RunOptions{nullptr},
    input_node_names.data(),
    inputTensors.data(),
    input_node_names.size(),
    output_node_names.data(),
    outputTensors.data(),
    output_node_names.size()
  );

  return outputTensorValues;

}
