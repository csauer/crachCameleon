// System include(s):
#include <iostream>
#include <regex>
#include <algorithm>
#include <map>


// ROOT-related includes
#include "TList.h"
#include "TFile.h"

// Custom imports
#include <deepSetsJSS/TrainTree.h>
#include "deepSetsJSS/TrainTreeAfterburner.h"
#include <deepSetsJSS/Preprocessing.h>
#include <deepSetsJSS/Utils.h>

// For sample normalization
#include "multijetsamples/LumiWeight.h"
#include "multijetsamples/filterWeight.h"


// Some global configs
using namespace preprocessing;


TrainTree :: TrainTree ()
  : m_fOutName("output.root"),
    m_treeName("TrackJSSTrees_JSS"),
    m_nEvents(0),
    m_branches
    {
      "truthJet_pt",
      "truthJet_eta",
      "truthJet_phi",
      "truthJet_E",
      "jet_pt",
      "jet_eta",
      "jet_phi",
      "jet_E",
      "tjet_constit_pt",
      "tjet_constit_eta",
      "tjet_constit_phi",
      "tjet_constit_E",
      "rjet_constit_pt",
      "rjet_constit_eta",
      "rjet_constit_phi",
      "rjet_constit_E",
      "jet_trk0_pt",
      "jet_trk0_eta",
      "jet_trk0_phi",
      "jet_trk0_E",
      "mcChannelNumber",
      "mcWeight",
      "jet_PartonTruthLabelID"
    }
{}


TrainTree :: ~TrainTree () {}


void TrainTree :: AddFile (const std::string & fName)
{

  // Show header if this is the first file that is added
  if ( !m_fNames.size() )
  {
    std::cout << "[INFO] The following file(s) are added to the RDF:" << std::endl;
  }

  // Add file to container
  m_fNames.push_back(fName);

  // Print last entry
  std::cout << "       |- " << m_fNames.back() << std::endl;

}


void TrainTree :: AddFile (const std::vector<std::string> & fNames)
{
  for (auto & fName : fNames)
  {
    this->AddFile( fName );
  }
}


void TrainTree :: AddCut (const std::string & cut)
{

  // Show header if this is the first cut that is added
  if ( !m_cuts.size() )
  {
    std::cout << "[INFO] The following cut(s) are added to the RDF:" << std::endl;
  }

  // Add file to container
  m_cuts.push_back(cut);

  // Print last entry
  std::cout << "       |- " << m_cuts.back() << std::endl;

}


void TrainTree :: AddCut (const std::vector<std::string> & cuts)
{
  for (auto & cut : cuts)
  {
    this->AddCut( cut );
  }
}


void TrainTree :: SetTreeName (const std::string & treeName)
{
  m_treeName = treeName;
  std::cout << "[INFO] The following tree will be read: " << m_treeName << std::endl;
}


void TrainTree :: SetNEvents (const int & nEvents)
{

  m_nEvents = static_cast<int>(nEvents / 2.0);

}


void TrainTree :: BookHisto (const TH1DModel & model, const std::string col, const std::string w)
{
//  m_modelH1D.push_back(model);
}


void TrainTree :: BookHisto (const TH1DModel & model, const std::string col1, const std::string col2, const std::string w)
{
//  m_modelH2D.push_back(model);
}


void TrainTree :: SetOutFileName (const std::string & fName)
{
  m_fOutName = fName;
  std::cout << "[INFO] Name of the output file: " << m_fOutName << std::endl;
}



auto TrainTree :: BasicFilter (RNode node, TRandom3 & rand)
{

//  .Filter("rdfentry_<10000")
  auto RDF_node = node
    // Fid. selection truth jet
    .Filter
    (
      [](const v_f & pt, v_f & eta )
      {
        if (pt.size() < 2)
        {
          return false;
        }
        if ( (TMath::Abs(eta[0])>2.1) || (TMath::Abs(eta[1])>2.1) )
        {
          return false;
        }
        if ( (pt[0]<60) || (pt[1]<60) )
        {
          return false;
        }
        if ( (pt[0]/pt[1] > 1.5) || (pt[0]>4500) )
        {
          return false;
        }
        return true;
      },
      {"truthJet_pt", "truthJet_eta"}
    )
    // Fid. selection reco jet
    .Filter
    (
      [](const v_f & pt, v_f & eta )
      {
        if (pt.size() < 2)
        {
          return false;
        }
        if ( (TMath::Abs(eta[0])>2.1) || (TMath::Abs(eta[1])>2.1) )
        {
          return false;
        }
        if ( (pt[0]<60) || (pt[1]<60) )
        {
          return false;
        }
        if ( (pt[0]/pt[1] > 1.5) || (pt[0]>4500) )
        {
          return false;
        }
        return true;
      },
      {"jet_pt", "jet_eta"}
    )
    .Filter
    (
      [](const bool & x){ return (x==true); },
      {"eventClean_LooseBad"}
    )
    .Define
    (
      "pos",
      []{return 0;}
    )
    .Define
    (
      "dsid",
      "mcChannelNumber"
    )
    .Define
    (
      "runnum",
      "RunNumber"
    );

  // Compute fraction of events to keep wrt to all events
  double frac = 1.0;
  if (m_nEvents != 0)
  {
    double ncount = static_cast<double>(RDF_node.Count().GetValue());
    if (ncount < m_nEvents)
    {
      m_nEvents = static_cast<int>(ncount / 2.0);
    }
    frac = static_cast<double>(m_nEvents) / ncount;
  }
  std::cout << "[INFO] Fraction: " << frac << std::endl;

  // Add cols
  return RDF_node
    .Define
    (
      "__jet_pt0",
      [](const v_f & x){return x[0];},
      {"jet_pt"}
    )
    .Define
    (
      "eventFraction",
      [&frac](){return frac;}
     )
    .Filter
     (
      [frac, &rand](const int & dsid, const int & runnum, const float & pt, const float & weight)
      {
        if (!MultijetSamples::IsValid(dsid, runnum, pt, weight))
        {
          return false;
        }
        double prob = rand.Uniform(0, 1);
        // Check if this event is valid
        if (frac > prob)
        {
          return true;
        }
        return false;
      },
      {"mcChannelNumber", "RunNumber", "__jet_pt0", "mcWeight"}
    );
}


auto TrainTree :: ApplyDefine (RNode node, const int & jetIdx)
{
  return node

    // Add feature/label
    .Define
    (
      "parton",
      [jetIdx](const std::vector<int> & x){return TMath::Abs(x[jetIdx]);},
      {"jet_PartonTruthLabelID"}
    )
    .Define
    (
      "target",
      [](const int & x)
      {
        if ( (x==11) || (x==21) )
        {
          return 1.0; // Gluons
        }
        else
        {
          return 0.0; // Quarks
        }
      },
      {"parton"}
    )

//    // Weights
//    .Define
//    (
//      "wFlatTJetPt",
//      []{return 1.f;}
//    )
//    .Define
//    (
//      "wFlatRJetPt",
//      []{return 1.f;}
//    )
//    .Define
//    (
//      "wTrainTJet",
//      []{return 1.f;}
//    )
//    .Define
//    (
//      "wTrainRJet",
//      []{return 1.f;}
//    )
//    .Define
//    (
//      "wFlatTarget",
//      []{return 1.f;}
//    )
    .Define
    (
      "wEventMC",
      "mcWeight"
    )
    .Define
    (
      "wEventJZ",
      [](const int & dsid, const int & runnum)
      {
        return MultijetSamples::GetNormalization(dsid, runnum);
      },
      {"mcChannelNumber", "RunNumber"}
    )
    .Define
    (
      "wEvent",
      "wEventMC * wEventJZ"
    )

    // - Total jet (reco)
    .Define
    (
      "rjet_pt",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"jet_pt"}
    )
    .Define
    (
      "rjet_eta",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"jet_eta"}
    )
    .Define
    (
      "rjet_phi",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"jet_phi"}
    )
    .Define
    (
      "rjet_e",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"jet_E"}
    )

    // - Total jet (truth)
    .Define
    (
      "tjet_pt",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"truthJet_pt"}
    )
    .Define
    (
      "tjet_eta",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"truthJet_eta"}
    )
    .Define
    (
      "tjet_phi",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"truthJet_phi"}
    )
    .Define
    (
      "tjet_e",
      [jetIdx](const v_f & x){return x[jetIdx];},
      {"truthJet_E"}
    )


    // - Constituents of the jet (nominal tracks) (reco)
    .Define
    (
      "rjetClus_pt",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"jet_trk0_pt"}
    )
    .Define
    (
      "rjetClus_eta",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"jet_trk0_eta"}
    )
    .Define
    (
      "rjetClus_phi",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"jet_trk0_phi"}
    )
    .Define
    (
      "rjetClus_e",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"jet_trk0_E"}
    )

    // - Constituents of the jet (nominal tracks) (truth)
    .Define
    (
      "tjetClus_pt",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"tjet_constit_pt"}
    )
    .Define
    (
      "tjetClus_eta",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"tjet_constit_eta"}
    )
    .Define
    (
      "tjetClus_phi",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"tjet_constit_phi"}
    )
    .Define
    (
      "tjetClus_e",
      [jetIdx](const v_v_f & x){return x[jetIdx];},
      {"tjet_constit_E"}
    )


    // Get sorted constituents (reco)
    .Define
    (
      "rjetSortClus_pt",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"rjetClus_pt", "rjetClus_pt"}
    )
    .Define
    (
      "rjetSortClus_eta",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"rjetClus_eta", "rjetClus_pt"}
    )
    .Define
    (
      "rjetSortClus_phi",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"rjetClus_phi", "rjetClus_pt"}
    )
    .Define
    (
      "rjetSortClus_e",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"rjetClus_e", "rjetClus_pt"}
    )

    // Get sorted constituents (truth)
    .Define
    (
      "tjetSortClus_pt",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"tjetClus_pt", "tjetClus_pt"}
    )
    .Define
    (
      "tjetSortClus_eta",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"tjetClus_eta", "tjetClus_pt"}
    )
    .Define
    (
      "tjetSortClus_phi",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"tjetClus_phi", "tjetClus_pt"}
    )
    .Define
    (
      "tjetSortClus_e",
      [](const v_f & x, const v_f & ref) {return sort_wrt(x, ref);},
      {"tjetClus_e", "tjetClus_pt"}
    )


    // Shift components (reco)
    .Define
    (
      "rjetSortClusCenter_eta",
      [](const v_f & x, const v_f & w) {return shift_eta(x, w);},
      {"rjetSortClus_eta", "rjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusCenter_eta"
    )
    .Define
    (
      "rjetSortClusCenter_phi",
      [](const v_f & x, const v_f & w) {return shift_phi(x, w);},
      {"rjetSortClus_phi", "rjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusCenter_phi"
    )

    // Shift components (truth)
    .Define
    (
      "tjetSortClusCenter_eta",
      [](const v_f & x, const v_f & w) {return shift_eta(x, w);},
      {"tjetSortClus_eta", "tjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusCenter_eta"
    )
    .Define
    (
      "tjetSortClusCenter_phi",
      [](const v_f & x, const v_f & w) {return shift_phi(x, w);},
      {"tjetSortClus_phi", "tjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusCenter_phi"
    )


    // PCA
    // - Truth jets
    .Define
    (
      "tjetAnglePCA",
      [](const v_f & eta, const v_f & phi, const v_f & e) {return pca_angle(eta, phi, e);},
      {"tjetSortClus_eta", "tjetSortClus_phi", "tjetSortClus_e"}
    )
    .Define
    (
      "tjetSortClusCenterRot_eta",
      [](const v_f & eta, const v_f & phi, const float & angle) {return  rot_eta(eta, phi, angle);},
      {"tjetSortClusCenter_eta", "tjetSortClusCenter_phi", "tjetAnglePCA"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusCenterRot_eta"
    )
    .Define
    (
      "tjetSortClusCenterRot_phi",
      [](const v_f & eta, const v_f & phi, const float & angle) {return  rot_phi(eta, phi, angle);},
      {"tjetSortClusCenter_eta", "tjetSortClusCenter_phi", "tjetAnglePCA"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusCenterRot_phi"
    )
    // Reco
    .Define
    (
      "rjetAnglePCA",
      [](const v_f & eta, const v_f & phi, const v_f & e) {return pca_angle(eta, phi, e);},
      {"rjetSortClus_eta", "rjetSortClus_phi", "rjetSortClus_e"}
    )
    .Define
    (
      "rjetSortClusCenterRot_eta",
      [](const v_f & eta, const v_f & phi, const float & angle) {return  rot_eta(eta, phi, angle);},
      {"rjetSortClusCenter_eta", "rjetSortClusCenter_phi", "rjetAnglePCA"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusCenterRot_eta"
    )
    .Define
    (
      "rjetSortClusCenterRot_phi",
      [](const v_f & eta, const v_f & phi, const float & angle) {return  rot_phi(eta, phi, angle);},
      {"rjetSortClusCenter_eta", "rjetSortClusCenter_phi", "rjetAnglePCA"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusCenterRot_phi"
    )

    // Flip eta
    // - Truth
    .Define
    (
      "tjetParity",
      [](const v_f & eta, const v_f & e) {return parity(eta, e);},
      {"tjetSortClusCenterRot_eta", "tjetSortClus_e"}
    )
    .Define
    (
      "tjetSortClusCenterRotFlip_eta",
      [](const v_f & eta, const int & parity) {return flip(eta, parity);},
      {"tjetSortClusCenterRot_eta", "tjetParity"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusCenterRotFlip_eta"
    )

    // - Reco
    .Define
    (
      "rjetParity",
      [](const v_f & eta, const v_f & e) {return parity(eta, e);},
      {"rjetSortClusCenterRot_eta", "rjetSortClus_e"}
    )
    .Define
    (
      "rjetSortClusCenterRotFlip_eta",
      [](const v_f & eta, const int & parity) {return flip(eta, parity);},
      {"rjetSortClusCenterRot_eta", "rjetParity"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusCenterRotFlip_eta"
    )


    // Normalize scalar components (reco)
    .Define
    (
      "rjetSortClusNormByPt_pt",
      [](const v_f & x, const v_f & ref) {return norm(x, ref);},
      {"rjetSortClus_pt", "rjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusNormByPt_pt"
    )
    .Define
    (
      "rjetSortClusNormByPt_e",
      [](const v_f & x, const v_f & ref) {return norm(x, ref);},
      {"rjetSortClus_e", "rjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "rjetSortClusNormByPt_e"
    )

    // Normalize scalar components (truth)
    .Define
    (
      "tjetSortClusNormByPt_pt",
      [](const v_f & x, const v_f & ref) {return norm(x, ref);},
      {"tjetSortClus_pt", "tjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusNormByPt_pt"
    )
    .Define
    (
      "tjetSortClusNormByPt_e",
      [](const v_f & x, const v_f & ref) {return norm(x, ref);},
      {"tjetSortClus_e", "tjetSortClus_pt"}
    )
    .Filter
    (
      [](const v_f & x){ return noNaNIsFinite(x); },
      "tjetSortClusNormByPt_e"
    );
}


auto TrainTree :: MkSnapshots (RNode node0, RNode node1)
{

  // Save the snapshots in a container to keep result pointer alive
  std::vector<SnapRet_t> rets;

  // Snapshot option
  ROOT::RDF::RSnapshotOptions opts;

  for (auto && colName : utils::drop_cols( node0.GetDefinedColumnNames()))
  {
    std::cout << "       |- " << colName << std::endl;
  }

  // Create a temporary directory
  utils::makedir(".tmp");

  // get hex string for tag
  std::string tag = utils::hex_string(10);

  // - Leading jet
  opts.fLazy = true;
  std::string fOut0( TString::Format(".tmp/TEMPORARY_SANAPSHOT_JET0_%s.root", tag.c_str()) );
  rets.push_back(node0.Snapshot(m_treeName, fOut0, utils::drop_cols(node0.GetDefinedColumnNames()), opts));

  // - Sub-leading jet
  opts.fLazy = false;
  std::string fOut1( TString::Format(".tmp/TEMPORARY_SANAPSHOT_JET1_%s.root", tag.c_str()) );
  rets.push_back(node1.Snapshot(m_treeName, fOut1, utils::drop_cols(node1.GetDefinedColumnNames())));

  // Merge those two files
  std::cout << "[INFO] Merging files ..." << std::endl;
  ROOT::RDataFrame RDF_out(m_treeName, {fOut0, fOut1});
  RDF_out.Snapshot(m_treeName, m_fOutName);

  // Delete tmp file(s)
  std::remove(fOut0.c_str());
  std::remove(fOut1.c_str());

}



void TrainTree :: Start ()
{

  // Run!
  ROOT::EnableImplicitMT(60);

  // Initialize the RDataFrame
  std::cout << "[INFO] Star reading RDF (this may take a while)" << std::endl;
  ROOT::RDataFrame RDF(m_treeName, m_fNames, m_branches);

  // Create a random number generator to sample randomly
  TRandom3 rand(0);

  // Apply basic selection to RDF
  auto RDF_basic = BasicFilter(RDF, rand);

  // Print some infos
  std::cout << "[INFO] Basic selection" << std::endl;
  for (auto && filtName : RDF_basic.GetFilterNames())
  {
    std::cout << "       |- " << filtName << std::endl;
  }

  // Create an RDF for leading (jetIdx=0) and sub-leading (jetIdx=1) jets
  auto RDF_0 = ApplyDefine(RDF_basic, 0);
  auto RDF_1 = ApplyDefine(RDF_basic, 1);

  // Print some infos for added cols
  std::cout << "[INFO] The following columns will be saved to the final ROOT file" << std::endl;
  for (auto && colName : utils::drop_cols(RDF_0.GetDefinedColumnNames()))
  {
    std::cout << "       |- " << colName << std::endl;
  }

  // Save data to ROOT file
  std::cout << "[INFO] Starting snapshots for leading and subleading jets" << std::endl;
  this->MkSnapshots(RDF_0, RDF_1);

  std::cout << "[INFO] Start reading histogram" << std::endl;
  // Get histograms
  this->ReadHistos();
  std::cout << "[INFO] Done!" << std::endl;

}


void TrainTree :: ReadHistos ()
{

  // Hash table
  std::map<std::string, std::vector<float> > hash =
  {
    {"NormByPt_pt", {100, 0.0, 1.0}},  {"NormByPt_e", {100, 0.0, 5.0}},
    {"_pt",         {100, 0.0, 4500}}, {"_e",         {100, 0.0, 5000}},
    {"_eta",        {100, -0.5, 0.5}}, {"_phi",       {100, -0.4, 0.4}},
    {"AnglePCA",    {50, -4.0, 4.0}},  {"Parity",     {5, -2.5, 2.5}},
    {"target",      {5, -2.0, 2.0}}
  };

  // Lits of histograms to be saved
  std::vector<ROOT::RDF::RResultPtr<::TH1D> > histos1d;
  std::vector<ROOT::RDF::RResultPtr<::TH2D> > histos2d;

  // Read the just created tree
  ROOT::RDataFrame RDF(m_treeName, m_fOutName);

  std::cout << "[INFO] Booking 1Dim histogram(s)" << std::endl;
  // Loop over all columns and create a histogram
  for (auto && col : RDF.GetColumnNames())
  {
    // Loop over hash table
    for (const auto & key : hash)
    {
      // Check if in hash table
      if (col.find(key.first) != std::string::npos)
      {
        // Book histograms
        histos1d.push_back
        (
          RDF.Histo1D
          (
            TH1DModel
            (
              TString::Format("h1_%s", col.c_str()).Data(),
              TString("").Data(),
              (int)hash[key.first][0],
              hash[key.first][1],
              hash[key.first][2]
            ),
            col, "wEvent"
          )
        );
        std::cout << "       |- " << histos1d.back()->GetName() << std::endl;
      }
    }
//    // Take default ranges for all other
//    histos1d.push_back(RDF.Histo1D(col));
//    histos1d.back()->SetName(TString::Format("h1_%s", histos1d.back()->GetName()).Data());
  }
  std::cout << "[INFO] Booking 2Dim histogram(s)" << std::endl;
  for (auto jet : {"tjet", "rjet"})
  {
    for (auto key : {"SortClus", "SortClusCenter", "SortClusCenterRot"})
    {
      histos2d.push_back
      (
        RDF.Histo2D
        (
          TH2DModel
          (
            TString::Format("h2_%s_%s_eta_vs_phi", jet, key).Data(),
            TString("").Data(),
            100, -0.5, +0.5, 100, -0.5, +0.5
          ),
          TString::Format("%s%s_eta", jet, key),
          TString::Format("%s%s_phi", jet, key),
          "wEvent"
        )
      );
      std::cout << "       |- " << histos2d.back()->GetName() << std::endl;
    }
    histos2d.push_back
    (
      RDF.Histo2D
      (
        TH2DModel
        (
          TString::Format("h2_%s_sortClusCenterRot_FlipEta_vs_phi", jet),
          TString(""),
          100, -0.5, +0.5, 100, -0.5, +0.5
        ),
        TString::Format("%sSortClusCenterRotFlip_eta", jet),
        TString::Format("%sSortClusCenterRot_phi", jet),
        "wEvent"
      )
    );
    std::cout << "       |- " << histos2d.back()->GetName() << std::endl;
  }

  std::cout << "[INFO] Start reading histograms" << std::endl;
  TFile fROOT(m_fOutName.c_str(), "UPDATE");
  fROOT.cd();
  for (auto & h : histos1d)
  {
    h->Write();
  }
  for (auto & h : histos2d)
  {
    h->Write();
  }
  fROOT.Close();
}
